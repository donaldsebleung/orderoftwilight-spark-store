#!/bin/bash -e

rm -rf ./dl-assets/
mkdir -p ./dl-assets/
pushd dl-assets/
wget https://github.com/Stabyourself/orderoftwilight/archive/refs/tags/1.1.tar.gz
tar xvf 1.1.tar.gz
pushd orderoftwilight-1.1/
zip -r orderoftwilight.love *
popd
popd
dpkg-buildpackage -us -uc
lintian ../orderoftwilight_1.1~spark3_all.deb
