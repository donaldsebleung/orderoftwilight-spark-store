# orderoftwilight-spark-store

## 介绍

[Order of Twilight](https://github.com/Stabyourself/orderoftwilight) deb 包，供[星火应用商店](https://spark-app.store/)上架

## 使用说明

于仓库根目录执行以下命令：

```bash
$ ./build-deb.sh
```

## 许可证

[MIT](./LICENSE)
